function starNestedLoop(m, n) {
  var space = "";
  for (let i = 0; i < n; i++) {
    space += "\n";
    for (let j = 0; j < m; j++) {
      space += "*";
    }
  }
  console.log(space);
}

//TEST CASE
console.log("starnestedloop (1,2)");
starNestedLoop(1, 2);
// *
// *
console.log("starnestedloop (2,3)");
starNestedLoop(2, 3);
// // **
// // **
// // **
console.log("starnestedloop (4,1)");
starNestedLoop(4, 1);
// ****
