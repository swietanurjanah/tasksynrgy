function starLoop(n) {
  for (let i = 0; i < n; i++) {
    console.log("*");
  }
  if (n < 0) {
    console.log("number invalid");
  }
}

// TEST CASE
console.log("starLoop(1)");
starLoop(1);
// *
console.log("starLoop(2)");
starLoop(2);

// *
// *
console.log("starLoop(6)");
starLoop(6);

//*
//*
//*
//*
//*
//*
console.log("starLoop(0)");
starLoop(0);
//
console.log("starLoop(-5)");
starLoop(-5);
// number invalid
